$(function(){
//Тут можете писать подобные функции связанные с авторизацией

  $('input').on('focus', function(){
    $('p.errorMessage').remove();
    $('input').removeClass('error');
  });


  //register
  $('#btnRegister').on('click', function (event) {
    event.preventDefault();

    var data = {
      username: $('#regUsername').val(),
      password: $("#regPassword").val(),
      passwordConfirm: $('#regPasswordConfirm').val(),
      email: $('#regEmail').val()
    };
    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/auth/register'
    }).done(function(data){
      if(!data.ok){
        $('.registerForm').after('<p class="errorMessage">' + data.error + '</p>');
        data.fields.forEach(function(item){
          $('input[name=' + item + ']').addClass('error');
        });
      } else {
        $('.registerForm').after('<p class="succes">Пользователь зарегистрирован!</p>');
        $('input').val('');
        $(location).attr('href','/user');
      }
    });
  });

  //login
  $('#btnLogin').on('click', function (event) {
    event.preventDefault();

    var data = {
      username: $('#logUsername').val(),
      password: $("#logPassword").val()
    };
    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/auth/login'
    }).done(function(data){
      if(!data.ok){
        $('.authorizationForm').after('<p class="errorMessage">' + data.error + '</p>');
        data.fields.forEach(function(item){
          $('input[name=' + item + ']').addClass('error');
        });
      } else {
        $('.authorizationForm').after('<p class="succes">Пользователь авторизован!</p>');
        $('input').val('');
        $(location).attr('href','user');
      }
    });
  });

  $('#btnLogout').on('click', function (event) {
    event.preventDefault();
    console.log('x');

    $.ajax({
      type: 'GET',
      //contentType: 'application/json',
      url: '/auth/logout'
    }).done(function(){
        $(location).attr('href','/');
    });
  });

  $('#btnChangePassword').on('click', function (event) {
    event.preventDefault();

    var data = {
      username: $(event.target).data('username'),
      passwordOld : $("#passwordOld").val(),
      password : $("#password").val(),
      passwordConfirm : $("#passwordConfirm").val()
    };
        console.log(data); 
    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/auth/changepassword'
    }).done(function(data){
      if(!data.ok){
        $('.userData').after('<p class="errorMessage">' + data.error + '</p>');
      } else {
        $('.userData').after('<p class="succes">Пароль успешно сменен!</p>');
        $(location).attr('href','/user');
      }
    });
  });

});