$(function(){

  //УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЯ АДМИНИСТРАТОРОМ
  $('.buttonDeleteUser').on('click', function (event) {
    event.preventDefault();

    var data = {
      username: $(event.target).data('username')
    };
    console.log(data); 
    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/administrator/deleteaccount'
    }).done(function(data){
      if(!data.ok){
        $('.userData').after('<p class="errorMessage">Упс... Что-то пошло не так ¯\_(ツ)_/¯</p>');
      } else {
        $('.userData').after('<p class="succes">Пользователь удален!</p>');
        $(location).attr('href','/administrator/users');
      }
    });
  });

  //ДОБАВЛЕНИЕ СЕРВИСА АДМИНИСТРАТОРОМ
  $('#btnAddService').on('click', function (event) {
  event.preventDefault();

  var data = {
    servicename: $('#iptServiceName').val(),
    access: $("#iptAccess").prop("checked")
  };
  console.log(data); 
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/administrator/addNewService'
  }).done(function(data){
    if(!data.ok){
      console.log('неудача');
      $('.serviceList').after('<p class="errorMessage">'+ data.error +'</p>');
    } else {
      console.log('удача');
      $('.serviceList').after('<p class="succes">Сервис добавлен!</p>');
      $(location).attr('href','/administrator/services');
    }
  });
  });

  //УДАЛЕНИЕ СЕРВИСА АДМИНИСТРАТОРОМ
  $('.buttonDeleteService').on('click', function (event) {
  event.preventDefault();

  var data = {
    servicename: $(event.target).data('servicename')
  };
  console.log(data); 
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/administrator/deleteservice'
  }).done(function(data){
    if(!data.ok){
      $('.serviceList').after('<p class="errorMessage">Упс... Что-то пошло не так ¯\_(ツ)_/¯</p>');
    } else {
      $('.serviceList').after('<p class="succes">Сервис удален!</p>');
      $(location).attr('href','/administrator/services');
    }
  });
  });

});