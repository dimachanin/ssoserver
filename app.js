//var https = require( "https" );  // для организации https
//var fs = require( "fs" );   // для чтения ключевых файлов

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const staticAsset = require('static-asset');
const mongoose = require('mongoose');
const session = require('express-session');
const mongoStore = require('connect-mongo')(session);

const config = require('./config');
const routes = require('./routes');
const models = require('./models');

//DATABASE CONNECTION
mongoose.Promise = global.Promise;
mongoose.set('debug', config.IS_PRODUCTION);

mongoose.connection
  .on('error', error => console.log(error))
  .on('close', () => console.log('database connection closed'))
  .once('open', () => {
    const info = mongoose.connection; //СТАРАЯ ВЕРСИЯ ПОСЛЕ => resolve(mongoose.connection)
    console.log(`Connected to ${info.host}:${info.port}/${info.name}`);
  });

mongoose.connect(config.MONGO_URL, { useNewUrlParser: true});

const user = require('./models/user');//модель пользователя
const service = require('./models/service');//модель сервиса

//EXPRESS
const app = express();

//Session
app.use(session({
    secret: config.SESSION_SECRET,
    resave: true,
    saveUninitialized: false,
    store: new mongoStore({
      mongooseConnection: mongoose.connection
    })
  })
);

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded( { extended:true } ));
app.use(bodyParser.json());
app.use(staticAsset(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));//подключение всех сторонних файлов, картинок, js'ок и css'ок
app.use('/javascripts', express.static(path.join(__dirname, 'node_modules', 'jquery', 'dist')));
app.use('/auth/', routes.auth);
//app.use('/services/', routes.services);
app.use('/administrator/', routes.administrator);

//ROUTERS

//GET ЗАПРОСЫ
app.get('/', (req, res) => {
  const username = req.session.username;
  if ( !username ) {
    res.render('index');
  } else {
    res.redirect('user');
  };

}); //рендер главной страницы
app.get('/user', (req, res) => {

  const id = req.session.userId;
  const username = req.session.username;
  const email = req.session.email;

  if ( !username ) {
    res.render('index');
  } else if ( username == 'admin') {
    res.render('administrator');
  } else {
    res.render('user', {
      user: {
        id,
        username,
        email
      }
    });
  }
});//рендер страницы пользователя 

app.get('/administrator', (req, res) => {

  const id = req.session.userId;
  const username = req.session.username;
  const email = req.session.email;

  if(!username || username !== 'admin'){
    res.render('index');
  } else {
    res.render('administrator');
  }
});//рендер страницы пользователя 

app.get('/registration', (req, res) => res.render('registration')); //рендер страницы регистрации

//ERROR HANDLER

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use((error, req, res, next) => { 
  res.status(error.status || 500);
  res.render('error', {
    message: error.message,
    error: !config.IS_PRODUCTION ? error : {},
    title: 'Oops...'
  });
});

//ЗАПУСК СЕРВЕРА
app.listen(config.PORT, ()=> 
  console.log(`App started on ${config.PORT} port`)
);

/*httpsOptions = {
    key: fs.readFileSync("server.key"), // путь к ключу
    cert: fs.readFileSync("server.cer") // путь к сертификату
}

https.createServer(httpsOptions, app).listen(443, ()=>{console.log(`App started on 443 port`)});
*/
