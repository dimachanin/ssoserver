const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const browserSync = require('browser-sync'); 
const concat = require('gulp-concat');
const uglify = require('gulp-uglifyjs');

gulp.task('scss', () => {
  return gulp
  .src([
    'src/scss/main.scss'
    ])
  .pipe(sass())
  .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'],{
     cascade:true
    })
   )
  .pipe(cssnano())
  .pipe(gulp.dest('public/stylesheets'));
});

gulp.task('scripts', () => {
  return gulp
    .src([
      'src/js/auth.js',
      'src/js/administrator.js'
      ])
    .pipe(concat('scripts.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/javascripts'))
});

gulp.task('watch', () => { 
  gulp.watch('src/scss/**/*.scss', gulp.series('scss'));
  gulp.watch('src/js/**/*.js', gulp.series('scripts')); 
});

gulp.task('default', gulp.series( 
  gulp.parallel('scss'), 
  gulp.parallel('scripts'),
  gulp.parallel('watch'),
));﻿

/*function bsr(done) { 
  browserSync.reload(); 
  done();
} 

gulp.task('browser-sync', () => {
  browserSync({
    server:{
      baseDir: 'dist'
    },
    notify: false
  });
});*/