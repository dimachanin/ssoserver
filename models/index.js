const user = require('./user');
const service = require('./service');

module.exports = {
  user,
  service
}