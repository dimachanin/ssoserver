const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  servicename:{
    type: String,
    require: true,
    unique: true
  },
  access:{
    type: Boolean,
    require: true
  }
  },{
    timestamps: true
  }
);

schema.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('services', schema);