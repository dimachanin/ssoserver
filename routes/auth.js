const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt-nodejs');

const models = require('../models');

//РЕГИСТРАЦИЯ
router.post('/register', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const passwordConfirm = req.body.passwordConfirm;
  const email = req.body.email;

  if(!username || !password || !passwordConfirm || !email){
    const fields = [];
    if (!username) fields.push('username');
    if (!password) fields.push('password');
    if (!passwordConfirm) fields.push('passwordConfirm');
    if (!email) fields.push('email');

    res.json({
      ok: false,
      error: 'Все поля должны быть заполнены!',
      fields
    });
  } else if (!/^[a-zA-Z0-9]+$/.test(username)){ //проверка на латынь 
    res.json({
      ok: false,
      error: 'Имя пользователя должно содеражать только латинские буквы и цифры!',
      fields: ['username']
    });
  } else if (username.length < 5 || username.length > 24){ //проверка на длину
    res.json({
      ok: false,
      error: 'Длина имени пользователя должна быть от 6 до 24 символов!',
      fields: ['username']
    });
  } else if (password !== passwordConfirm){ //проверка на совпадение
    res.json({
      ok: false,
      error: 'Пароли не совпадают!',
      fields: ['password', 'passwordConfirm']
    });
  } else if (password.length < 8){ //проверка на совпадение
    res.json({
      ok: false,
      error: 'Длина пароля не менее 8 символов',
      fields: ['password', 'passwordConfirm']
    });
  } else if (!/^[0-9a-z_-]+@[0-9a-z_-]+\.[a-z]{2,5}$/i.test(email)){ //проверка на почту
    res.json({
      ok: false,
      error: 'Почта введена неккоректно!',
      fields: ['email']
    });
  } else {

    models.user.findOne({username}).then(user => {
      if(!user){

        //Кодирование пароля и занесение данных в БД
        bcrypt.hash(password, null, null, (err, hash) => {
        models.user.create({ //создание пользователя по модели
          username, 
          password: hash, 
          email
        }).then(user => {
          console.log(user);
          req.session.userId = user.id; //установка кук сессии
          req.session.username = user.username;
          req.session.email = user.email;
          res.json({
            ok: true
          });
        }).catch(err => {
          console.log(err);
          res.json({
            ok: false,
            error: 'Ошибка, попробуйте позже'
          });
          });
        });

      } else {
        
        //Если имя занято показываем ошибку
        res.json({
          ok: false,
          error: 'Имя занято',
          fields: ['username']
        });

      }
    });
  }
});

//ВХОД В АККАУНТ
router.post('/login', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  if(!username || !password){
    const fields = [];
    if (!username) fields.push('username');
    if (!password) fields.push('password');

    res.json({
      ok: false,
      error: 'Все поля должны быть заполнены!',
      fields
    });
  } else {
    models.user.findOne({username}).then(user => {
      if (!user){
        res.json({
          ok:false,
          error: 'Имя пользователя и пароль не совпадают!',
          fields: ['username', 'password']
        });
      } else {
        bcrypt.compare(password, user.password, function(err, result){
          if(!result){
            res.json({
              ok:false,
              error: 'Имя пользователя и пароль не совпадают!',
              fields: ['username', 'password']
            });
          } else {
            req.session.userId = user.id;
            req.session.username = user.username;
            req.session.email = user.email;
            res.json({
              ok: true
            });
          }
        });
      }
    }).catch(err => {
          console.log(err);
          res.json({
            ok: false,
            error: 'Ошибка, попробуйте позже'
          });
        });
  }
});

//ВЫХОД ИЗ АККАУНТА
router.get('/logout', ( req, res ) => {
  if(req.session){
    req.session.destroy(() => {
      res.redirect('/');
    });
  } else {
      res.redirect('/');
  }
});

//СМЕНА ПАРОЛЯ 
router.post('/changepassword', ( req, res ) => {
  const username = req.body.username;
  const passwordOld = req.body.passwordOld;
  const password = req.body.password;
  const passwordConfirm = req.body.passwordConfirm;

  models.user.findOne( { username } ).then( user => {
    bcrypt.compare( passwordOld, user.password, function( err, result ){
      console.log(result);
      if(!result){
        res.json({
          ok:false,
          error: 'Введен неверный старый пароль!'
        });
      } else if( password !== passwordConfirm){
        res.json({
          ok:false,
          error: 'Новые пароли не совпадает!'
        });
      } else if( password.length < 8 ){
        res.json({
          ok:false,
          error: 'Пароль слишком короткий!'
        });  
      } else {   
        bcrypt.hash(password, null, null, (err, hash) => {
          models.user.updateOne({username : username}, {password : hash}, (req, result) => {
            if(!result){ 
              res.json({
                ok: false,
                error: 'Упс... что-то пошло не так!'
              });
            } else {
            res.json({
              ok: true
            });
            }
            console.log(result);
          });
        });
      }
    });

    });
});

module.exports = router;