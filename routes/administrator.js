const express = require('express');
const router = express.Router();

const models = require('../models');

//СПИСОК СЕРВИСОВ
router.get('/services', (req, res) => { //рендер страницы пользователи, доступно только администратору

  const username = req.session.username;
    //В ОБЩЕМ ТУТ НАДО ЧТО-ТО ДУМАТЬ ТАК КАК ЕСТЬ БАГ С РОУТЕРАМИ И АДРЕСОМ URL

  if(username == 'admin'){
    models.service.find({}).then(services => {
      res.render('services', { services : services});
    });
  }
}); 

//ДОБАВЛЕНИЕ НОВОГО СЕРВИСА
router.post('/addNewService', (req, res) => {
  const servicename = req.body.servicename;
  const access = req.body.access;

  //if (servicename !== шаблону) ПРОЕРКА НА ШАБЛОН
  models.service.findOne({servicename}).then(service => {
    if(!service){
      models.service.create({ //создание пользователя по модели
        servicename, 
        access
      }).then(service => {
        console.log(service);
        res.json({
          ok: true
        });
      }).catch(err => {
        console.log(err);
        res.json({
          ok: false,
          error: 'Ошибка, попробуйте позже'
        });
      }); 
    } else {
      res.json({
        ok: false,
        error: 'Ошибка, такой сервис уже есть!'
      });
    }

  });
});

//УДАЛЕНИЕ СЕРВИСА
router.post('/deleteservice', (req, res) => {
    const servicename = req.body.servicename;

    models.service.remove({servicename : servicename}, (req, result) => {
    if(!result){ 
      res.json({
        ok: false
      });
    } else {
      res.json({
        ok: true
      });
    }
    console.log(result);
  });
});

//УДАЛЕНИЕ АККАУНТА АДМИНИСТРАТОРОМ
router.post('/deleteaccount', ( req, res ) => {
  const username = req.body.username;

  models.user.remove({username : username}, (req, result) => {
    if(!result){ 
      res.json({
        ok: false
      });
    } else {
      res.json({
        ok: true
      });
    }
    console.log(result);
  });
});

//СПИСОК ПОЛЬЗОВАТЕЛЕЙ
router.get('/users', (req, res) => { //рендер страницы пользователи, доступно только администратору

  const username = req.session.username;
    //В ОБЩЕМ ТУТ НАДО ЧТО_ТО ДУМАТЬ ТАК КАК ЕСТЬ БАГ

  if(username == 'admin'){
    models.user.find({}).then(users => {
      res.render('users', { users : users});
    });
  }

}); 

module.exports = router;